from tkinter import *
from tkinter.ttk import Combobox
from textblob import TextBlob
from tkinter import messagebox
root = Tk()

root.geometry("500x300")
root.resizable(False,False)
root.title("I_D_K")
root.configure(bg = "black")

#####list

list_1={'afrikaans': 'af', 'albanian': 'sq', 'amharic': 'am', 'arabic': 'ar', 'armenian': 'hy', 'azerbaijani': 'az', 'basque': 'eu', 'belarusian': 'be', 'bengali': 'bn', 'bosnian': 'bs', 'bulgarian': 'bg', 'catalan': 'ca', 'cebuano': 'ceb', 'chichewa': 'ny', 'chinese (simplified)': 'zh-cn', 'chinese (traditional)': 'zh-tw', 'corsican': 'co', 'croatian': 'hr', 'czech': 'cs', 'danish': 'da', 'dutch': 'nl', 'english': 'en', 'esperanto': 'eo', 'estonian': 'et', 'filipino': 'tl', 'finnish': 'fi', 'french': 'fr', 'frisian': 'fy', 'galician': 'gl', 'georgian': 'ka', 'german': 'de', 'greek': 'el', 'gujarati': 'gu', 'haitian creole': 'ht', 'hausa': 'ha', 'hawaiian': 'haw', 'hebrew': 'he', 'hindi': 'hi', 'hmong': 'hmn', 'hungarian': 'hu', 'icelandic': 'is', 'igbo': 'ig', 'indonesian': 'id', 'irish': 'ga', 'italian': 'it', 'japanese': 'ja', 'javanese': 'jw', 'kannada': 'kn', 'kazakh': 'kk', 'khmer': 'km', 'korean': 'ko', 'kurdish (kurmanji)': 'ku', 'kyrgyz': 'ky', 'lao': 'lo', 'latin': 'la', 'latvian': 'lv', 'lithuanian': 'lt', 'luxembourgish': 'lb', 'macedonian': 'mk', 'malagasy': 'mg', 'malay': 'ms', 'malayalam': 'ml', 'maltese': 'mt', 'maori': 'mi', 'marathi': 'mr', 'mongolian': 'mn', 'myanmar (burmese)': 'my', 'nepali': 'ne', 'norwegian': 'no', 'odia': 'or', 'pashto': 'ps', 'persian': 'fa', 'polish': 'pl', 'portuguese': 'pt', 'punjabi': 'pa', 'romanian': 'ro', 'russian': 'ru', 'samoan': 'sm', 'scots gaelic': 'gd', 'serbian': 'sr', 'sesotho': 'st', 'shona': 'sn', 'sindhi': 'sd', 'sinhala': 'si', 'slovak': 'sk', 'slovenian': 'sl', 'somali': 'so', 'spanish': 'es', 'sundanese': 'su', 'swahili': 'sw', 'swedish': 'sv', 'tajik': 'tg', 'tamil': 'ta', 'telugu': 'te', 'thai': 'th', 'turkish': 'tr', 'ukrainian': 'uk', 'urdu': 'ur', 'uyghur': 'ug', 'uzbek': 'uz', 'vietnamese': 'vi', 'welsh': 'cy', 'xhosa': 'xh', 'yiddish': 'yi', 'yoruba': 'yo', 'zulu': 'zu'}
# combo box

lavaf= StringVar()
font_box = Combobox(root,width=30,textvariable=lavaf,state="readonly")
font_box['values'] = [x for x in list_1.keys()]
font_box.current(37)
font_box.place(x=300,y=0)

# Enter box

# main commandyiyt

def exit():
    rr =messagebox.askyesnocancel("notification","ARE YOU WANT TO EXIT!!!/",parent=root)
    if rr ==True:
        root.destroy()


def click():
    try:
        word2 = TextBlob(v_l.get())
        lan = word2.detect_language()
        lan_todit = lavaf.get()
        lan_to = list_1[lan_todit]
        word2 = word2.translate(from_lang = lan ,to= lan_to)
        lable3.configure(text= word2)
        v_l_1.set(word2)
    except:
        v_l_1.set("Key not support in choice choice Language")
   
#### bind
def on_enterentry(e):
    entry["bg"]="springgreen"

def on_leaveentry(e):
    entry["bg"]="white"

def on_enter2entry(e):
    entry_2["bg"]="springgreen"

def on_leave2entry(e):
    entry_2["bg"]="white"
### button
def on_enterb1(e):
    b1["bg"]="blue"

def on_leaveb1(e):
    b1["bg"]="pink"

def on_enterb2(e):
    b2["bg"]="blue"

def on_leaveb2(e):
    b2["bg"]="pink"


v_l = StringVar()

entry = Entry(root,width=31,textvariable=v_l,font=("times",15,"italic bold"))
entry.place(x=150,y=40)

v_l_1 = StringVar()

entry_2 = Entry(root,width=31,textvariable=v_l_1,font=("times",15,"italic bold"),relief="ridge")
entry_2.place(x=150,y=100)

## lable

lable = Label(root,text="Enter Words:",font=("times",15,"italic bold"),bg="red")
lable.place(x=5,y=40)


lable2 = Label(root,text="OutPut Words:",font=("times",15,"italic bold"),bg="red")
lable2.place(x=5,y=100)


lable3 = Label(root,text="",font=("times",15,"italic bold"),bg='black')
lable3.place(x=10,y=250)

##### Button

b1 = Button(root,text="Translat",bd=3,bg="pink",width=10,font=("times",15,"italic bold"),command=click)
b1.place(x=40,y=170)

b2 = Button(root,text="Exit=>",bd=3,bg="pink",width=10,font=("times",15,"italic bold"),command=exit)
b2.place(x=280,y=170)

#b1 = Button(root,text="Translat",bd=3,bg="pink",width=10,font=("times",15,"italic bold"))
#b1.place(x=40,y=170)

entry.bind('<Enter>',on_enterentry)
entry.bind('<Leave>',on_leaveentry)

entry_2.bind('<Enter>',on_enter2entry)
entry_2.bind('<Leave>',on_leave2entry)

b1.bind('<Enter>',on_enterb1)
b1.bind('<Leave>',on_leaveb1)

b2.bind('<Enter>',on_enterb2)
b2.bind('<Leave>',on_leaveb2)



root.mainloop()